// Initialize leaflet.js
var L = require('leaflet');

// Initialize the map
var map = L.map('map', {
    scrollWheelZoom: true
});

// Set the position and zoom level of the map
map.setView([51.13, 12.3], 12);

// Initialize the base layer
var osm_mapnik = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; OSM Mapnik <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

// marker

const marker = L.marker([51.13,12.3]).addTo(map);

// gh api basics
const axios = require('axios')
const api_url = 'http://185.183.159.250:8989/isochrone';

let firstTime = true;

// onclick function

async function onMapClick(e) {


    const lat = e.latlng.lat;
    const lng = e.latlng.lng
    console.log([lat,lng].join())



    marker.setLatLng([lat,lng]);
    if (firstTime) {
        map.setView([lat,lng],10);
        firstTime = false;
    }
    console.log([lat,lng]);
    document.getElementById('lat').textContent = lat.toFixed(4);
    document.getElementById('lon').textContent = lng.toFixed(4);

    const config = {
        url: api_url,
        method: 'GET',
        header: {
            'Content-Type': 'application/json'
        },
        params: {
            point: [lat,lng].join(),
            time_limit: '600',
            vehicle: 'bike',
            weighting: 'short_fastest',
        }
    }
    axios(config)
        .then(function(response){
            console.log(response.data);
            L.geoJson(response.data.polygons).addTo(map)
        })

}

//initialize onclick funtionality

map.on('click', onMapClick)




// gh iso API
