// initialize node-postgres
const { Client } = require('pg')
const client = new Client({
    user: "iprk",
    password: "test1234",
    host: "185.183.159.250",
    port: 5432,
    database: "sandbox"
})

client.connect()
// Initialize leaflet.js
var L = require('leaflet');
require('leaflet-draw')
require('leaflet-sidebar-v2')

// Initialize the map
var map = L.map('map', {
    drawControl: true
});

// Set the position and zoom level of the map
map.setView([51.341236, 12.345], 14);

// Initialize the base layer
var osm_mapnik = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; OSM Mapnik <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

// marker

let marker = L.marker([51.341,12.358]).addTo(map);

// sidebar

var sidebar = L.control.sidebar({
    autopan: false,       // whether to maintain the centered map point when opening the sidebar
    closeButton: true,    // whether t add a close button to the panes
    container: 'sidebar', // the DOM container or #ID of a predefined sidebar container that should be used
    position: 'left',     // left or right
}).addTo(map);

// draw functionality

let drawnItems = L.featureGroup().addTo(map);

// sidebar dummy content

marker.addEventListener("click", function(){
    sidebar.addPanel('Das ist ein Test');
},false)

// sidebar for submitting data

function createSidebar() {
    let sidebarHtmlString =
        '<form>' +
        '<label for="input_cat">Kategorie: </label><br>\n' +
        '<select id="input_cat" name="modes" >\n' +
        '  <option value="bike">Radverkehr</option>\n' +
        '  <option value="foot">Fußverkehr</option>\n' +
        '  <option value="sharing">Carsharing</option>\n' +
        '  <option value="parking">Parkplätze</option>\n' +
        '</select><br> ' +
        'Name:<br><input type="text" id="input_name"><br>' +
        'Textbeschreibung:<br> <textarea name="message" rows="10" cols="30" id="input_desc">\n' +
        'lorem ipsum dolor sit amet \n' +
        '</textarea><br>' +
        '  <label for="input_img">Select image:</label><br>' +
        '  <input type="file" id="input_img" name="input_img" accept="image/*">\n' +
        '<input type="button" value="Submit" id="submit">' +
        '</form>';

    var sidebarContent = {
        id: 'userinfo',
        tab: 'test',
        pane: sidebarHtmlString,
        title: 'test',
    }
    sidebar.addPanel(sidebarContent)
    sidebar.open('userinfo')

}

// draw controls
map.addEventListener("draw:created", function(e) {
    e.layer.addTo(drawnItems);
    drawnItems.eachLayer(function(layer) {
        let geojson = JSON.stringify(layer.toGeoJSON().geometry);
        console.log(geojson);
    });
    createSidebar()

});

// what happens if you hit 'submit'

function setData(e) {

    if(e.target && e.target.id == "submit") {

        // Get category, user name and description
        let enteredCategory = document.getElementById("input_cat").value;
        let enteredUsername = document.getElementById("input_name").value;
        let enteredDescription = document.getElementById("input_desc").value;
        let enteredImage = document.getElementById("input_img").value;

        // Print user name and description
        console.log(enteredCategory);
        console.log(enteredUsername);
        console.log(enteredDescription);
        console.log(enteredImage)

        // Get and print GeoJSON for each drawn layer
        drawnItems.eachLayer(function(layer) {
            let drawing = JSON.stringify(layer.toGeoJSON().geometry);
            console.log(drawing);
        });

        // Clear drawn items layer
        sidebar.removePanel('userinfo');
        drawnItems.clearLayers();

    }

}

document.addEventListener("click", setData);

if(e.target && e.target.id == "submit") {
    map.addEventListener("draw:editstart", function(e) {
        sidebar.removePanel('userinfo');
    });
    map.addEventListener("draw:deletestart", function(e) {
        sidebar.removePanel('userinfo');
    });
    map.addEventListener("draw:editstop", function(e) {
        sidebar.addPanel('userinfo');
    });
    map.addEventListener("draw:deletestop", function(e) {
        if(drawnItems.getLayers().length > 0) {
            sidebar.addPanel('userinfo');
        }
    });
}

